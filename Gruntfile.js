'use strict';
var lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet;
var mountFolder = function (connect, dir) {
  return connect.static(require('path').resolve(dir));
};

module.exports = function (grunt) {
  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').concat(['gruntacular']).forEach(grunt.loadNpmTasks);

  // configurable paths
  var yeomanConfig = {
    app: 'app',
    dist: 'q'
  };

  var taskConfig, buildConfig, runConstants, environment;

  buildConfig = require('./build.config.js');

  environment = process.env.NODE_ENV || 'prod';
  if (environment.trim().toLowerCase() === 'production') {
    environment = 'prod';
  } else if (environment.trim().toLowerCase() === 'development') {
    environment = 'dev';
  }
  grunt.log.ok('environment set to ' + environment);

  runConstants = (function() {
    var baseRunConstants = buildConfig.config_dir + 'config.json';
    var envRunConstants = buildConfig.config_dir + 'config.' + environment + '.json';
    return grunt.util._.extend(grunt.file.readJSON(baseRunConstants), grunt.file.readJSON(envRunConstants));
  })();

  try {
    yeomanConfig.app = require('./component.json').appPath || yeomanConfig.app;
  } catch (e) {}

  taskConfig = {
    yeoman: yeomanConfig,
    ngconstant: {
      options: {
        name: '<%= angular_appname %>.constants',
        wrap: '<%= __ngModule %>',
        constants: {
          settings: runConstants
        }
      },
      server: {
        options: {
          dest: '.tmp/scripts/constants.js'
        }
      },
      dist: {
        options: {
          dest: '.tmp/scripts/constants.js' //useminPrepare will pick this up from .tmp
        }
      }
    },
    watch: {
      options: {
        livereload: true
      },
      html: {
        files: ['<%= yeoman.app %>/{,*/}*.html']
      },
      cssmin: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.css'],
        tasks: ['cssmin:dist']
      },
      constants: {
        files: ['<%= config_dir %>*.json'],
        tasks: ['ngconstant:server']
      },
      jshint: {
        files: ['<%= yeoman.app %>/scripts/{,*/,*/*/,*/*/*/}*.js'],
        tasks: ['jshint:app']
      }
    },
    connect: {
      proxies: [
        {
          context: ['/user', '/oauth', '/saml', '/services', '/tos', '/transfertypes', '/login.html'],
          host: 'localhost',
          port: 9999
        }
      ],
      livereload: {
        options: {
          port: 9000,
          // Change this to '0.0.0.0' to access the server from outside.
          hostname: '*',
          middleware: function (connect) {
            return [
              lrSnippet,
              require('grunt-connect-proxy/lib/utils').proxyRequest,
              mountFolder(connect, '.tmp'),
              mountFolder(connect, yeomanConfig.app),
              mountFolder(connect, yeomanConfig.dist)
            ];
          }
        }
      },
      test: {
        options: {
          port: 9000,
          middleware: function (connect) {
            return [
              mountFolder(connect, '.tmp'),
              mountFolder(connect, 'test')
            ];
          }
        }
      }
    },
    clean: {
      dist: ['.tmp', '<%= yeoman.dist %>/*'],
      server: '.tmp'
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      gruntfile: [
        'Gruntfile.js'
      ],
      app: [
        '<%= yeoman.app %>/scripts/{,*/}*.js'
      ]
    },
    concat: {
      dist: {
        files: {
          '<%= yeoman.dist %>/scripts/scripts.js': [
            '.tmp/scripts/*.js',
            '<%= yeoman.app %>/scripts/*.js'
          ]
        }
      }
    },
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>',
        root: [
          '<%= yeoman.app %>',
          '.tmp'
        ]

      }
    },
    usemin: {
      html: ['<%= yeoman.dist %>/{,*/}*.html'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      options: {
        dirs: ['<%= yeoman.dist %>']
      }
    },
    cssmin: {
      dist: {
        files: {
          // '<%= yeoman.dist %>/styles/main.css': [
          //   '<%= yeoman.app %>/styles/*.css'
          // ],
          '<%= yeoman.dist %>/styles/1177.css': [
            '<%= yeoman.app %>/styles/1177/style.css',
            '<%= yeoman.app %>/styles/1177/widgets.css'
          ],
          '<%= yeoman.dist %>/styles/main.css': [
            '<%= yeoman.app %>/styles/normalize.css',
            '<%= yeoman.app %>/styles/style.css'
          ]
        }
      }
    },
    htmlmin: {
      dist: {
        options: {},
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>',
          src: ['*.html', 'views/{,*/}*.html', 'template/{,*/}*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/*.html']
      }
    },
    ngmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>/scripts',
          src: '*.js',
          dest: '<%= yeoman.dist %>/scripts'
        }]
      }
    },
    uglify: {
      dist: {
        files: {
          '<%= yeoman.dist %>/scripts/scripts.js': [
            '<%= yeoman.dist %>/scripts/scripts.js'
          ]
        }
      }
    },
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,txt}',
            '.htaccess',
            'components/**/*',
            'img/**/*'
          ]
        }]
      }
    }
  };

  grunt.initConfig(grunt.util._.extend(taskConfig, buildConfig));

  grunt.registerTask('server', [
    'clean:server',
    'cssmin:dist',
    'ngconstant',
    'configureProxies:server',
    'connect:livereload',
    'watch'
  ]);

  grunt.registerTask('test', [
    'clean:server',
    'connect:test',
    'testacular'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    // 'jshint:app',
//    'test',
    /*'replace:dist',*/
    'cssmin:dist',
    'ngconstant:dist',
    'useminPrepare',
    'htmlmin',
    'concat',
    'copy',
    'usemin',
    'ngmin',
    'uglify'
  ]);

  grunt.registerTask('default', ['build']);
};
