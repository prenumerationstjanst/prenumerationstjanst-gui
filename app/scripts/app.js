'use strict';

var pumpApp = angular.module('pumpApp', ['ngCookies','ui.router', 'angularModalService', 'pumpApp.constants']);

pumpApp.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
  function($stateProvider, $urlRouterProvider, $httpProvider) {

    function withPreCheck(routeConfig) {
      routeConfig.resolve = routeConfig.resolve || {};
      //make sure precheck is required for all functions
      //this makes use of the fact that we use the array notation for all resolve functions
      _.mapValues(routeConfig.resolve, function(value) {
        if (_.isArray(value)) {
          value.splice(value.length-1,0, 'preCheck'); //add preCheck as the last dependency (defined below)
        }
        return value;
      });
      routeConfig.resolve.preCheck = ['PreCheckService', function(PreCheckService) {
        return PreCheckService.preCheck();
      }];
      return routeConfig;
    }

    $stateProvider.state(withPreCheck({
      name: 'home',
      url: '/',
      templateUrl: 'views/home.html',
      controller: 'HomeCtrl',
      abstract: true,
      resolve: {
        subscriptions: ['SubscriptionService',
          function(SubscriptionService) {
            return SubscriptionService.getSubscriptions();
          }
        ],
        transferTypes: ['TransferTypeService',
          function(TransferTypeService) {
            return TransferTypeService.loadTransferTypes();
          }
        ],
        profile: ['UserService', 
          function(UserService) {
            return UserService.getPersonalInfo(true);
          }
        ]
      }
    }))
    .state(withPreCheck({
      name: 'home.subscribe',
      url: '',
      templateUrl: 'views/home.subscribe.html',
      controller: 'HomeSubscribeCtrl'
    }))
    .state(withPreCheck({
      name: 'home.active',
      url: 'active',
      templateUrl: 'views/home.active.html',
      controller: 'HomeActiveCtrl'
    }))
    .state(withPreCheck({
      name: 'subscription-confirmation',
      url: '/subscription-confirmation/:key',
      templateUrl: 'views/subscription-confirmation.html',
      controller: 'SubscriptionConfirmationCtrl',
      titleKey: 'subscription_confirmation.title',
      resolve: {
        confirmation: ['SubscriptionService', '$stateParams', 
          function(SubscriptionService, $stateParams) {
            return SubscriptionService.getSubscriptionConfirmation($stateParams.key);
          }
        ]
      }
    }))
    .state({
      name: '500',
      url: '/500',
      templateUrl: 'views/500.html'
    })
    .state({
      name: 'age-restriction',
      url: '/age-restriction',
      templateUrl: 'views/age-restriction.html'
    });

    $urlRouterProvider.otherwise('/home');

    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRF-TOKEN';
    $httpProvider.defaults.xsrfCookieName = 'CSRF-TOKEN';
  }
]);

pumpApp.run(['$rootScope', '$location', '$timeout', 'UserService',
  function($rootScope, $location, $timeout, UserService) {

    $rootScope.lang = 'sv';
    $rootScope.loading = true;

    $rootScope.currentPath = $location.path();

    UserService.checkLoggedIn().then(function() {
      UserService.getPersonalInfo(true).then(function(personalInfo) {
        $rootScope.personalInfo = personalInfo;
      });
    }, function(error) { //not logged in
      console.error('not logged in error', error);
      $location.path('/500');
    });

    var timeoutPromise = null;

    var startLoading = function() {
      if (timeoutPromise !== null) {
        $timeout.cancel(timeoutPromise);
      }
      timeoutPromise = $timeout(function() {
        $rootScope.loading = true;
      }, 400);
    };

    var stopLoading = function() {
      if (timeoutPromise !== null) {
        $timeout.cancel(timeoutPromise);
      }
      timeoutPromise = null;
      $rootScope.loading = false;
    };

    $rootScope.$on('$stateChangeStart', function() {
      startLoading();
    });

    $rootScope.$on('$stateChangeSuccess', function() {
      stopLoading();
      $rootScope.currentPath = $location.path();
    });

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
      stopLoading();
      //TODO: handle route change errors, old code messes with 'bootstrap' above
      if (angular.isDefined(error) && error.correlationId) {
        $rootScope.errorCid = error.correlationId;
      }
      $location.path('/500');
    });

    $rootScope.$on('age-restriction-error', function() {
      $location.path('/age-restriction');
    });

    $rootScope.$on('create-subscription-error', function() {
      $location.path('/500');
    });

    $rootScope.$on('error', function() {
      $location.path('/500');
    });
  }
]);