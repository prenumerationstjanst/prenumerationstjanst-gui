'use strict';

angular.module('pumpApp').controller('SubscriptionConfirmationCtrl', ['$scope', 'confirmation',
  function($scope, confirmation) {
    $scope.confirmation = confirmation;
  }
]);
