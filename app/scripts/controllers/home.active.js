'use strict';

angular.module('pumpApp').controller('HomeActiveCtrl', ['$scope', '$state', '$timeout', '$location', 'ScrollToItemService', 'SubscriptionService', 'HomeErrorService',
  function($scope, $state, $timeout, $location, ScrollToItemService, SubscriptionService, HomeErrorService) {
    $scope.stopSubscriptions = function() {
      if (!_validateStopSubscriptions()) {
        $timeout(function() {
          ScrollToItemService.scrollToItem('#all-errors');
        }, 0);
        return;
      }
      var transferTypes = _.keys(_.pick($scope.stopFormData.transferTypes, function(value) {
        return value; //value is boolean
      }));
      console.log('transferTypes', transferTypes);
      SubscriptionService.deleteSubscription(transferTypes).then(function(resp) {
        if (resp && resp.subscriptionConfirmationKey) {
          $location.path('/subscription-confirmation/' + resp.subscriptionConfirmationKey);
        } else {
          console.info('reloading state');
          $state.reload();
        }
      });
    };

    /**
     * When user performs action on the stopFormData objects,
     * try to uncheck errors. We can not call _validateStopSubscriptions
     *  since the user might not have actually tried to submit yet
     * 
     */
    $scope.$watch('stopFormData', function() {
      _tryUncheckErrors();
    }, true);

    var _validateStopSubscriptions = function() {
      HomeErrorService.setStopErrors({
        transferTypes: !_areStopTransferTypesValid()
      });
      return !HomeErrorService.getStopErrors().transferTypes;
    };

    var _areStopTransferTypesValid = function() {
      return _areFormDataTransferTypesValid($scope.stopFormData.transferTypes);
    };

    var _areFormDataTransferTypesValid = function(formDataTransferTypes) {
      return !!_.keys(_.pick(formDataTransferTypes, function(value) {
        return value; //value is boolean
      })).length;
    };

    var _tryUncheckErrors = function() {
      if (_areStopTransferTypesValid()) {
        HomeErrorService.setStopErrors({
          transferTypes: false
        });
      }
    };

    var _resetStopErrors = function() {
      HomeErrorService.reset();
    };

    var _resetStopFormData = function() {
      $scope.stopFormData = {};
    };

    _resetStopFormData();
    _resetStopErrors();


  }
]);
