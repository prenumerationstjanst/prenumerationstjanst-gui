'use strict';

angular.module('pumpApp').controller('HomeSubscribeCtrl', ['$scope', '$q', '$state', '$timeout', '$window', '$location', 'ScrollToItemService', 'SubscriptionService', 'PreCheckService', 'ModalService', 'HomeErrorService', 'profile',
  function($scope, $q, $state, $timeout, $window, $location, ScrollToItemService, SubscriptionService, PreCheckService, ModalService, HomeErrorService, profile) {

    $scope.transferTypeFilter = function() {
      return function(transferType) {
        for (var i = 0; i < $scope.subscriptions.length; i += 1) {
          var subscription = $scope.subscriptions[i];
          var subTransferType = subscription.approvedTransferType;
          if (transferType === subTransferType) {
            return false;
          }
        }
        return true;
      };
    };

    $scope.startSubscriptions = function() {
      if (!_validateStartSubscriptions()) {
        $timeout(function() {
          ScrollToItemService.scrollToItem('#all-errors');
        }, 0);
        return;
      }
      _performPreCheck().then(function() {
        var transferTypes = _.keys(_.pick($scope.startFormData.transferTypes, function(value) {
          return value; //value is boolean
        }));
        console.log('transferTypes', transferTypes);
        SubscriptionService.createSubscription(transferTypes).then(function(resp) {
          if (resp && resp.phkActionUrl) { //phk not authorized, must redirect
            console.info('must redirect to ' + resp.phkActionUrl);
            $window.location.href = resp.phkActionUrl;
          } else if (resp && resp.subscriptionConfirmationKey) { //phk already authorized, go to confirmation
            $location.path('/subscription-confirmation/' + resp.subscriptionConfirmationKey);
          } else {
            $state.reload();
            $window.scrollTo(0,0);
          }
        });
      }, function() { //preCheck error (User may have de-authorized in phk after loading home)
        console.info('precheck failed');
        $state.reload();
        $window.scrollTo(0,0);
      });
    };

    $scope.showTermsModal = function() {
      ModalService.showModal({
        templateUrl: 'views/terms-modal.html',
        controller: function($scope, close) {
          $scope.close = close;
        }
      });
    };

    /**
     * When user performs action on the startFormData objects,
     * try to uncheck errors. We can not call _validateStartSubscriptions
     *  since the user might not have actually tried to submit yet
     * 
     */
    $scope.$watch('startFormData', function() {
      _tryUncheckErrors();
    }, true);

     var _performPreCheck = function() {
      var deferred = $q.defer();
      if (profile.phkAuthorized) { //UI depends on this flag.
        PreCheckService.preCheck().then(function(result) {
          if (result) {
            console.log('pc true');
            deferred.resolve();
          } else {
            console.log('pc false');
            deferred.reject();
          }
        }, function() { //error
          console.log('pc error');
          deferred.reject();
        });
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    };

    var _validateStartSubscriptions = function() {
      var tmpStartErrors = {
        transferTypes: !_areStartTransferTypesValid(),
        acceptTerms: !_isTermsOfServiceAcceptanceValid()
      };
      HomeErrorService.setStartErrors(tmpStartErrors);
      return !tmpStartErrors.transferTypes && !tmpStartErrors.acceptTerms;
    };

    var _areStartTransferTypesValid = function() {
      return _areFormDataTransferTypesValid($scope.startFormData.transferTypes);
    };

    var _isTermsOfServiceAcceptanceValid = function() {
      return $scope.startFormData.acceptTerms;
    };

    var _areFormDataTransferTypesValid = function(formDataTransferTypes) {
      return !!_.keys(_.pick(formDataTransferTypes, function(value) {
        return value; //value is boolean
      })).length;
    };

    var _tryUncheckErrors = function() {
      var _tmpStartErrors = HomeErrorService.getErrors().startErrors;
      if (_areStartTransferTypesValid()) {
        _tmpStartErrors.transferTypes = false;
      }
      if (_isTermsOfServiceAcceptanceValid()) {
        _tmpStartErrors.acceptTerms = false;
      }
      HomeErrorService.setStartErrors(_tmpStartErrors);
    };

    var _resetStartErrors = function() {
      HomeErrorService.reset();
    };

    var _resetStartFormData = function() {
      $scope.startFormData = {
        acceptTerms: false
      };
    };

    _resetStartFormData();
    _resetStartErrors();


  }
]);
