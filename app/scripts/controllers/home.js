'use strict';

angular.module('pumpApp').controller('HomeCtrl', ['$scope', 'HomeErrorService', 'subscriptions', 'transferTypes', 'profile',
  function($scope,HomeErrorService, subscriptions, transferTypes, profile) {
    console.log('HOMEHOME');

    $scope.$watch(HomeErrorService.getErrors, function(errors) {
      $scope.errors = errors;
    }, true);

    //used by both home.subscribe / home.active
    $scope.subscriptions = _.filter(subscriptions, function(subscription) {
      return subscription.complete;
    });

    $scope.transferTypes = transferTypes;
    $scope.phkAuthorized = profile.phkAuthorized;

  }
]);
