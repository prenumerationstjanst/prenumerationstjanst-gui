//
// Modified version of code from http://stackoverflow.com/a/24577533
// by 'Liad Livnat' (http://stackoverflow.com/users/345944/liad-livnat)
//
// usage example: <a href="" scroll-to-item="#someIdOnThePage">scroll to #someIdOnThePage</a>
//
'use strict';

angular.module('pumpApp').directive('scrollToItem', function () {
  return {
    restrict: 'A',
    scope: {
      scrollToItem: '@'
    },
    link: function (scope, $elm) {
      $elm.on('click', function (event) {
        event.preventDefault();
        $('html,body').animate({ scrollTop: $(scope.scrollToItem).offset().top }, 'slow');
      });
    }
  };
});

angular.module('pumpApp').factory('ScrollToItemService', 
  function() {
    var _scrollToItem = function(selector) {
      $('html,body').animate({scrollTop: $(selector).offset().top}, 'slow');
    };
    return {
      scrollToItem: _scrollToItem
    };
  }
);