/**
 * Directive to load message (via message directive)
 * based on titleKey key of $state.current (typically specified in $stateProvider.state block)
 */
'use strict';

angular.module('pumpApp').directive('currentTitle', ['$state',
  function ($state) {
    var extractKey = function() {
      return (typeof $state.current.titleKey !== 'undefined') ? $state.current.titleKey : 'title';
    };
    return {
      restrict: 'E',
      scope: true,
      replace: true,
      //need to place the message directive in an extra span, otherwise angular complains
      template: '<span><message key="{{theKey}}" nbo></span></span>',
      link: function ($scope) {
        $scope.$on('$stateChangeSuccess', function() {
          $scope.theKey = extractKey();
        });
        $scope.theKey = extractKey();
      }
    };
  }
]);