'use strict';

angular.module('pumpApp').directive('infoToggle', ['$rootScope',
  function ($rootScope) {
    return {
      restrict: 'A',
      replace: true,
      transclude: true,
      scope: true,
      template: '<i class="info-toggle open" ng-class="{active:active}" ng-click="click()" ng-transclude></i>',
      link: function ($scope, element, attr) {
        $scope.active = false;
        $scope.click = function () {
          $scope.active = !$scope.active;
          $rootScope.$broadcast('info-toggle-' + $scope.key, $scope.active);
        };
        attr.$observe('infoKey', function (key) {
          $scope.key = key;
        });
      }
    };
  }]
);

angular.module('pumpApp').directive('infoToggleInfo', ['$rootScope',
  function ($rootScope) {
    return {
      restrict: 'A',
      replace: true,
      transclude: true,
      scope: true,
      template: '<p class="info-toggle-info" ng-show="show" ng-transclude></p>',
      link: function ($scope, element, attr) {
        $scope.show = false;

        attr.$observe('key', function (key) {
          $rootScope.$on('info-toggle-' + key, function (ev, isActive) {
            $scope.show = isActive;
          });
        });
      }
    };
  }]
);

