'use strict';

angular.module('pumpApp').directive('pointyLink', [
  function() {
    return {
      restrict: 'EA',
      templateUrl: 'views/directives/1177PointyLink.html',
      replace: true,
      transclude: true
    };
  }
]);

