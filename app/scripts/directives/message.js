'use strict';

/**
 * message directive for externalizing text resources.
 *
 * All resourcekeys are expected to be defined in lowercase
 * Also supports dynamic key values such as key="status.{{scopedvalue}}"
 *
 * Usage: <message key="some.resource.key"
 * [fallback="defaulttextifnokeyfound"]/>
 */
angular.module('pumpApp').directive('message', ['$rootScope', '$sce', '$timeout', 'MessageService',
  function ($rootScope, $sce, $timeout, MessageService) {
    return {
      restrict: 'EA',
      scope: true,
      replace: true,
      template: '<span ng-bind-html="resultValue"></span>',
      link: function ($scope, element, attr) {
        // observe changes to interpolated attribute
        attr.$observe('key', function (interpolatedKey) {
          $scope.resultValue = MessageService.getMessage(interpolatedKey, attr.fallback);
          var isNotBindOnce = angular.isDefined(attr.nbo);
          if(!isNotBindOnce) {
            $timeout(function() {
              $scope.$destroy();
            });
          }
        });
      }
    };
  }
]);

angular.module('pumpApp').factory('MessageService', ['$rootScope', '$sce',
  function($rootScope, $sce) {
    // drilldown through all my.message.key structure
    var _getPropertyValue = function(resources, propertyKey) {
      var splitKey = propertyKey.split('.');
      while (splitKey.length && (resources = resources[splitKey.shift()])) {
      }
      return resources;
    };

    var _getMessage = function(key, fallback) {
      var normalizedKey = angular.lowercase(key);
      //messages is a global object
      var value = _getPropertyValue(messages[$rootScope.lang], normalizedKey);
      var returnValue;
      if (typeof value !== 'undefined') {
        returnValue = value;
      } else if (typeof fallback !== 'undefined') {
        returnValue = fallback;
      } else {
        returnValue = '[Missing ' + normalizedKey + ']';
      }
      return $sce.trustAsHtml(returnValue);
    };

    return {
      getMessage: _getMessage
    };
  }
]);

