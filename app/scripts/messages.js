'use strict';
window.messages = {
  sv: {
    title: 'Prenumerera på journalinformation till Hälsa för mig',
    loading: 'Sidan laddas...',
    home: {
      active_subscriptions: 'Aktiva prenumerationer',
      create_subscription: 'Starta prenumeration',
      errors: {
        general_title: 'Din begäran kunde tyvärr inte genomföras. Åtgärda de markerade felen och försök igen.'
      },
      start_errors: {
        transfer_types: 'Välj journalinformation som du vill prenumera på.',
        accept_terms: 'Godkänn villkoren.'
      },
      stop_errors: {
        transfer_types: 'Välj journalinformation som du vill avsluta prenumeration för'
      },
      region_info: {
        text: '<strong>OBS!</strong> För närvarande kan journalinformation endast hämtas från följande vårdgivare:<br> Region Jönköpings län och Landstinget i Värmland. <a class="link link1177" href="#">Läs mer på 1177.se<img src="img/1177/icon-newwindow.png" class="extlink" alt="Länken öppnas i nytt fönster."></a>.',
        link_text: 'Läs mer på 1177.se'
      },
      subscription_table: {
        header: {
          select: 'Välj',
          transfer_type: 'Journalinformation',
          latest_transfer: 'Senaste överföring'
        },
        latest_transfer_status: {
          never: 'Aldrig',
          requested: 'Begärd'
        }
      },
      terms: {
        title: 'Villkor',
        cond_1: 'Endast signerad och vidimerad journalinformation hämtas till <a class="link link1177" href="#">Hälsa för mig<img src="http://uxa.se/img/e1177/icon-newwindow.png" class="extlink" style="margin-left:4px; padding-bottom:3px;" alt="Länken öppnas i nytt fönster."></a>.',
        cond_2: 'Om du har förseglat journalinformation i din journal på nätet innebär det att det inte längre finns någon möjlighet att logga in i e-tjänsten Journalen. Förseglad journalinformation hämtas inte till Hälsa för mig.',
        cond_3: 'Du ansvarar själv för journalinformationen som hämtats och vem som får tillgång till den via Hälsa för mig.',
        cond_4: 'E-hälsomyndigheten ansvarar för lagringen av din journalinformation i Hälsa för mig.',
        cond_5: 'Du kan när som helst välja att avsluta prenumerationen. Om du väljer att avsluta prenumerationen kommer journalinformation som redan finns i Hälsa för mig att finnas kvar där.',
        accept_checkbox: 'Jag godkänner villkoren', //TODO: remove
        accept_terms_pre_popup_text: 'Jag godkänner',
        accept_terms_popup_text: 'villkoren'
      },
      phk_authorization_info: {
        text: '<p>Du behöver nu ansluta till Hälsa för mig som tar emot din journalinformation.</p><p>Du kommer skickas vidare till inloggning hos Hälsa för mig, där du också behöver skapa ett konto om du inte redan har gjort det. När du har anslutit kommer du sedan automatiskt tillbaka hit.</p>'
      },
      create_transfer_btn: {
        phk_authorized: 'Starta prenumeration',
        phk_not_authorized: 'Fortsätt till Hälsa för mig'
      },
      stop_transfer_btn: 'Avsluta prenumeration'
    },
    subscription_confirmation: {
      title: 'Bekräftelse'
    },
    transfer_types: {
      care_documentation: {
        title: 'Journalanteckningar'
      },
      care_contacts: {
        title: 'Vårdkontakter'
      },
      maternity_medical_history: {
        title: 'Mödravårdsjournal'
      },
      vaccination_history: {
        title: 'Vaccinationer'
      },
      laboratory_order_outcome: {
        title: 'Provsvar (klinisk kemi)'
      },
      diagnosis: {
        title: 'Diagnoser'
      }
    },
    500: {
      title: 'Ett oväntat fel inträffade',
      desc: 'Gå till startsidan eller kontakta supporten.',
      error_reference_pre_reference: 'Referens att uppge i kontakt med supporten: ',
      home_btn: 'Till startsidan'
    },
    age_restriction: {
      title: 'Du är för ung för att använda tjänsten',
      desc: 'För att använda tjänsten måste du vara minst 18 år gammal.',
    },
    mvk: {
      home: 'Start',
      other_services: 'Övriga tjänster'
    }
  }
};
