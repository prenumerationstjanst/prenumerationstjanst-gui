'use strict';
var app = angular.module('pumpApp');
var COOKIE_NAME = 'CSRF-TOKEN';
app.factory('csrfCookieInterceptor', ['$cookies',
  function($cookies) {
    return {
      request: function(config) {
        var cookieVal = (Date.now().toString(36) +  Math.random().toString(36).substr(2, 5)).toUpperCase();
        $cookies.put(COOKIE_NAME, cookieVal);
        return config;
      },
      response: function(response) {
        $cookies.remove(COOKIE_NAME);
        return response;
      },
      responseError: function(response) {
        $cookies.remove(COOKIE_NAME);
        return response;
      }
    };
  }
]);

app.config(['$httpProvider', 
  function($httpProvider) {
    $httpProvider.interceptors.push('csrfCookieInterceptor');
  }
]);