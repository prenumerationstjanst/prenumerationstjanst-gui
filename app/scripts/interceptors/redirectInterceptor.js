'use strict';
var app = angular.module('pumpApp');

app.factory('redirectInterceptor', ['$window',
  function($window) {
    return {
      response: function(response) {
        if ((typeof response.data === 'string' || response.data instanceof String)) {
          if (response.data.includes('RelayState')) { //still on local server
            console.info('RelayState detected');
            //TODO: display some message to user?
            $window.location.reload();
          } else if (response.data.includes('/mg-local/login')) { //Probably at IdP
            console.info('/mg-local/login detected');
            //TODO: display some message to user?
            $window.location.reload();
          }
        }
        return response;
      }
    };
  }
]);

app.config(['$httpProvider', 
  function($httpProvider) {
    $httpProvider.interceptors.push('redirectInterceptor');
  }
]);