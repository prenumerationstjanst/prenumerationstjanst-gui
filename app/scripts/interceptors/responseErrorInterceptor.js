'use strict';
var app = angular.module('pumpApp');

app.factory('responseErrorInterceptor', ['$rootScope', '$q',
  function($rootScope, $q) {
    return {
      responseError: function(errorResponse) {
        console.log('rejection', errorResponse);
        if (errorResponse && errorResponse.data) {
          var errorCode;
          if (errorResponse.data.message) { //spring security
            errorCode = errorResponse.data.message;
          } else if (errorResponse.data.errorCode) {
            errorCode = errorResponse.data.errorCode;
          }
          if (errorCode) {
            if (errorCode === 'AGE_RESTRICTION') {
              $rootScope.$emit('age-restriction-error');
              return $q.reject();
            }
          }
        }
        return $q.reject({
          data: errorResponse.data,
          status: errorResponse.status,
          statusText: errorResponse.statusText,
          correlationId: errorResponse.headers('X-PT-CLIENT-CORRELATION-ID')
        });
      }
    };
  }
]);

app.config(['$httpProvider', 
  function($httpProvider) {
    $httpProvider.interceptors.push('responseErrorInterceptor');
  }
]);