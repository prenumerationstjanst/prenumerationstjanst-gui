'use strict';
angular.module('pumpApp').factory('TermsOfServiceService', ['$http', '$q',
  function($http, $q) {

    var termsOfServiceService = {};

    termsOfServiceService.retrieveTos = function() {
      var deferred = $q.defer();
      $http.get('/tos').then(function(result) {
        if (angular.isDefined(result.data)) {
          deferred.resolve(result.data);
        } else {
          deferred.reject('could not obtain terms of service');
        }
      });
      return deferred.promise;
    };

    return termsOfServiceService;
  }
]);
