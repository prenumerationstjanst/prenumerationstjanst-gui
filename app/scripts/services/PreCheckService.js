'use strict';
angular.module('pumpApp').factory('PreCheckService', ['$http', '$q',
  function($http, $q) {

    var _preCheck = function() {
      var deferred = $q.defer();
      $http.get('/user/precheck',{cache: false}).then(function(result) {
        console.log('precheck result', result.data);
        deferred.resolve(result.data);
      }, function() { //TODO: handle errors
        console.error('precheck failed');
        deferred.reject();
      });
      return deferred.promise;
    };

    return {
      preCheck: _preCheck
    };
  }
]);
