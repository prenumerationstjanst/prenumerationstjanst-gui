'use strict';
angular.module('pumpApp').factory('HomeErrorService', [
  function() {

    var _startErrorsBase = {
      acceptTerms: false,
      transferTypes: false
    };

    var _stopErrorsBase = {
      transferTypes: false
    };

    var _errors = {
      startErrors: _.clone(_startErrorsBase),
      stopErrors: _.clone(_stopErrorsBase)
    };

    return {
      getErrors: function() {
        return _errors;
      },
      setStartErrors: function(startErrors) {
        _.assign(_errors.startErrors, startErrors);
      },
      getStartErrors: function() {
        return _errors.startErrors;
      },
      setStopErrors: function(stopErrors) {
        _.assign(_errors.stopErrors, stopErrors);
      },
      getStopErrors: function() {
        return _errors.stopErrors;
      },
      reset: function() {
        this.setStartErrors(_startErrorsBase);
        this.setStopErrors(_stopErrorsBase);
      }
    };
  }
]);
