'use strict';
angular.module('pumpApp').factory('SubscriptionService', ['$rootScope', '$http', '$q', '$log', 'settings',
  function($rootScope, $http, $q, $log, settings) {

    var subscriptions;
    var lastReload = -1;

    var _getSubscriptions = function() {
      var deferred = $q.defer();
      var currentTime = new Date().getTime();
      $log.debug('lastReload', lastReload, 'settings.subscription_reload_interval', settings.subscription_reload_interval, 'currentTime', currentTime);
      var doReload = currentTime > (lastReload + settings.subscription_reload_interval);
      if (doReload) {
        $log.debug('reloading subscriptions from backend');
        $http.get('/user/subscriptions').then(function(result) {
          if (angular.isDefined(result.data)) {
            subscriptions = result.data;
            deferred.resolve(subscriptions);
            lastReload = currentTime;
          } else {
            deferred.resolve([]);
          }
        });
      } else if (angular.isDefined(subscriptions) && subscriptions.length > 0) {
        $log.debug('not reloading subscriptions from backend');
        deferred.resolve(subscriptions);
      } else {
        $log.error('could not load subscriptions');
        deferred.reject('could not load subscriptions');
      }
      return deferred.promise;
    };

    var _getSubscription = function(subscriptionId) {
      var deferred = $q.defer();
      _getSubscriptions().then(function(subscriptions) {
        var found = false;
        for (var i = 0; i < subscriptions.length; i += 1) {
          if (subscriptions[i].id === subscriptionId) {
            deferred.resolve(subscriptions[i]);
            found = true;
            break;
          }
        }
        if (!found) {
          deferred.reject('subscription not found');
        }
      });
      return deferred.promise;
    };

    var _createSubscription = function(transferTypes) {
      console.info('_createSubscription transferTypes[' + transferTypes + ']');
      var deferred = $q.defer();
      $http.post('/user/subscriptions',
        {
          transferTypes: transferTypes
        }).then(function(result) {
          if (angular.isDefined(result.data)) {
            console.debug(result.data);
            deferred.resolve(result.data);
          } else {
            deferred.resolve();
          }
        }, function(error) {
          console.error('error', error);
          deferred.reject();
        });
      return deferred.promise;
    };

    var _deleteSubscription = function(transferTypes) {
      console.info('_deleteSubscription transferTypes[' + transferTypes + ']');
      var deferred = $q.defer();
      $http.post('/user/subscriptions/delete',
        {
          transferTypes: transferTypes
        }).then(function(result) {
          if (angular.isDefined(result.data)) {
            console.debug(result.data);
            deferred.resolve(result.data);
          } else {
            deferred.resolve();
          }
        }, function(error) {
          console.error('error', error);
          deferred.reject();
        });
      return deferred.promise;
    };

    var _getSubscriptionConfirmation = function(key) {
      console.debug('_getSubscriptionConfirmation key[' + key + ']');
      var deferred = $q.defer();
      $http.get('/user/subscriptions/status', {
        params: {
          key: key
        }
      }).then(function(result) {
          if (angular.isDefined(result.data)) {
            console.debug(result.data);
            deferred.resolve(result.data);
          } else {
            deferred.reject();
          }
        }, function() {
          console.error('error getting confirmation');
          deferred.reject();
        });
      return deferred.promise;
    };

    return {
      getSubscription: _getSubscription,
      createSubscription: _createSubscription,
      getSubscriptions: _getSubscriptions,
      deleteSubscription: _deleteSubscription,
      getSubscriptionConfirmation: _getSubscriptionConfirmation,
    };
  }
]);
