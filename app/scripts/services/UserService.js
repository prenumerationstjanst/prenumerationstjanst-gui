'use strict';
angular.module('pumpApp').factory('UserService', ['$http', '$log', '$q', '$interval', 'settings',
  function($http, $log, $q, $interval, settings) {

    var personalInfo;
    var lastReload = -1;

    var _cachedPIDeferred;
    var _getPersonalInfo = function(forceReload) {
      if (!_cachedPIDeferred) {
        $log.debug('no cache');
        _cachedPIDeferred = $q.defer();
        var currentTime = new Date().getTime();
        $log.debug('lastReload', lastReload, 'settings.personal_info_reload_interval', settings.personal_info_reload_interval, 'currentTime', currentTime);
        var doReload = !!forceReload || (currentTime > (lastReload + settings.personal_info_reload_interval));
        if (doReload) {
          $log.debug('reloading personalInfo from backend');
          $http.get('/user/profile').then(function(result) {
            personalInfo = _.pick(result.data, ['name', 'phkAuthorized']);
            $log.debug('personalInfo', personalInfo);
            _cachedPIDeferred.resolve(personalInfo);
            lastReload = currentTime;
          }).catch(function() { //error
            if (angular.isDefined(personalInfo)) {
              $log.warn('could not load personalInfo from server, returning old personalInfo');
              _cachedPIDeferred.resolve(personalInfo);
            } else {
              _cachedPIDeferred.reject('could not load personalInfo from server and no old personalInfo cached');
            }
          }).finally(function() {
            _cachedPIDeferred = null;
          });
        } else if (angular.isDefined(personalInfo)) {
          $log.debug('not reloading personalInfo from backend');
          _cachedPIDeferred.resolve(personalInfo);
        } else {
          _cachedPIDeferred.reject('could not load personalInfo');
        }
      } else {
        $log.debug('call already in progress to /user/profile');
      }
      return _cachedPIDeferred.promise;
    };

    var _checkLoggedIn = function() {
      var deferred = $q.defer();
      $http.get('/user/profile').then(function(result) {
        console.log('checkLoggedIn', result);
        if (result.status === 302) {
          //SP wants to redirect us, this should mean that we are not logged in any more
          deferred.reject('not-logged-in');
        } else {
          deferred.resolve();
        }
      }, function(error) {
        deferred.reject(error);
      });
      return deferred.promise;
    };

    return {
      getPersonalInfo: _getPersonalInfo,
      checkLoggedIn: _checkLoggedIn
    };
  }
]);
