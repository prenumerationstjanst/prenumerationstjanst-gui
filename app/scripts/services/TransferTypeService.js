'use strict';
angular.module('pumpApp').factory('TransferTypeService', ['$http', '$q',
  function($http, $q) {

    var _loadTransferTypes = function() {
      var deferred = $q.defer();
      $http.get('/transfertypes', {cache: true}).then(function(result) {
        deferred.resolve(result.data);
      });
      return deferred.promise;
    };

    var transferTypeService = {};

    transferTypeService.loadTransferTypes = function() {
      return _loadTransferTypes();
    };

    return transferTypeService;
  }
]);
